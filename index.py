import pandas as pd
import os
import re
import urllib.request
from werkzeug.utils import secure_filename
from flask import Flask, make_response, \
    redirect, request, flash, url_for, \
    send_from_directory, render_template
"""
How does it work
1. Place csv file onto files folder
2. Select Column to look for.
3. Now, the script prints on console. Later on, the script is going to
save on a csv file as well.
Recall activate a  virtual env with requirements.txt as 'source venv/bin/activate'
"""

# ENDS_FILE = re.compile(r"\\.(csv|xls)$")

app = Flask(__name__,
            instance_relative_config=False,
            template_folder="templates",
            static_folder="static")

# ALLOWED_EXTENSIONS = {'txt', 'pdf' 'csv', 'xls'}
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'xlsx', 'xls', 'csv'])
UPLOAD_FOLDER = 'static/uploads'
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def generate_data_frame(file):
    ctn_type = file.content_type
    if(ctn_type == 'application/csv'):
        return pd.read_csv(file)
    if(ctn_type == 'application/vnd.ms-excel'):
        return pd.read_excel(file)
    return -1


@app.route('/')
def home():
    return make_response({'response': 'Funciona!'}, 200)


@app.route('/upload', methods=["POST", "GET"])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No file selected for uploading')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            flash('File successfully uploaded')
            dataframe = generate_data_frame(file)
            if(dataframe is not -1):
                return render_template('thanks.html', data=pd.to_json(dataframe))
            # return redirect(url_for('uploaded_file', filename=filename))
        else:
            flash('Allowed file types are txt, pdf, png, jpg, jpeg, gif')
            return redirect(request.url)

    return render_template("index.html")


@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)


csv_file_names = []
for csv_files in filter(lambda x: x.endswith('.csv'), os.listdir('./files')):
    csv_file_names.append(csv_files)


# Convert a csv file to a pandas dataframe which is simpler to work with
# and offers ease to pull data from it.

data = []
for file in csv_file_names:
    file_name = pd.read_csv(os.path.join(
        os.getcwd(), "files", "{0}".format(file)))
    data.append(file_name)


# select the column to pull out to.
columns = [
    "Name",
    "Price"
]
# In this case, just take out index 0 or first file.

for column in columns:
    print(data[0][column])  # Recall that print takes out the first 20 records.
    print(data[0][column].to_json())

if __name__ == "__main__":
    app.secret_key = 'super secret key'
    app.debug = True
    app.run()
